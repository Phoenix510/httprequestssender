﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HTTPRequests
{
    static class WebHandler
    {
        private static readonly HttpClient client = new HttpClient();

        #region HeaderOperations
        public static bool AddHeader(string name, string value)
        {
            var exists = client.DefaultRequestHeaders.Contains(name);
            if (!exists)
                client.DefaultRequestHeaders.Add(name, value);
            else
                return false;
            return true;
        }

        public static bool RemoveHeader(string name)
        {
            var exists = client.DefaultRequestHeaders.Contains(name);
            if (exists)
                client.DefaultRequestHeaders.Remove(name);
            else
                return false;
            return true;
        }
        #endregion

        #region Post
        public static async Task<(string content, int statusCode)> PostAsync(string uri, string toSend)
        {
            return await ErrorHandler(postFunc, new object[] { uri, toSend });
        }

        public static async Task<(string content, int statusCode)> PostAsync(string uri, HttpContent toSend)
        {
            return await ErrorHandler(postFunc, new object[] { uri, toSend });
        }

        static Func<object[], Task<HttpResponseMessage>> postFunc = delegate (object[] args)
        {
            if (args[1] is HttpContent)
                return client.PutAsync(args[0].ToString(), args[1] as HttpContent);

            StringContent data = new StringContent($"{args[1]}", Encoding.UTF8, "application/json");
            return client.PostAsync(args[0].ToString(), data);
        };
        #endregion

        #region Put
        public static async Task<(string content, int statusCode)> PutAsync(string uri, string toSend)
        {
            return await ErrorHandler(putFunc, new object[] { uri, toSend });
        }

        static Func<object[], Task<HttpResponseMessage>> putFunc = delegate (object[] args)
        {
            StringContent data = new StringContent((string)args[1], Encoding.UTF8, "application/json");
            return client.PutAsync(args[0].ToString(), data);
        };
        #endregion

        #region Get
        public static async Task<(string content, int statusCode)> GetAsync(string uri)
        {
            return await ErrorHandler(getFunc, new object[] { uri });
        }

        static Func<object[], Task<HttpResponseMessage>> getFunc = delegate (object[] args)
        {
            return client.GetAsync(args[0].ToString());
        };
        #endregion

        #region Delete
        public static async Task<(string content, int statusCode)> DeleteAsync(string uri)
        {
            return await ErrorHandler(deleteFunc, new object[] { uri });
        }

        static Func<object[], Task<HttpResponseMessage>> deleteFunc = delegate (object[] args)
        {
            return client.DeleteAsync(args[0].ToString());
        };
        #endregion

        public static bool IsSuccessStatusCode(int code)
        {
            if (code > 199 && code < 300) return true;
            else return false;
        }

        private static async Task<(string content, int statusCode)> ErrorHandler(Func<object[], Task<HttpResponseMessage>> fuction, object[] args)
        {
            try
            {
                var result = await fuction(args);
                return (result.Content.ReadAsStringAsync().Result, (int)result.StatusCode);
            }
            catch (Exception ex)
            {
                return (ex.Message, 0);
            }
        }
    }
}
