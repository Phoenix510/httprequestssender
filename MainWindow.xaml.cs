﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HTTPRequests
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // item1 = headername | item2 = value
        public ObservableCollection<Tuple<string, string>> Headers = new ObservableCollection<Tuple<string, string>>();

        public List<string> Requests = new List<string>()
        {
            "GET",
            "POST",
            "PUT",
            "DELETE"
        };

        public MainWindow()
        {
            InitializeComponent();
            comboBoxRequest.ItemsSource = Requests;
            HeadersList.ItemsSource = Headers;
        }

        private async void Send_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(URL.Text))
            {
                (string response, int statusCode) result = ("unknown", 0);
                switch (comboBoxRequest.SelectedItem.ToString())
                {
           
                    case "GET":
                        result = await WebHandler.GetAsync(URL.Text);
                        break;
                    case "POST":
                        result = await WebHandler.PostAsync(URL.Text, body.Text);
                        break;
                    case "PUT":
                        result = await WebHandler.PutAsync(URL.Text, body.Text);
                        break;
                    case "DELETE":
                        result = await WebHandler.DeleteAsync(URL.Text);
                        break;
                    default:
                        result = await WebHandler.GetAsync(URL.Text);
                        break;
                }
                response.Text = string.Empty;
                response.Text = FormatTextToJson(result.response);
                ResponseCode.Text = $"Response code: {result.statusCode}"; 
            }
            else
            {
                MessageBox.Show("Uzupełnij adres URL!", "Error");
            }
        }

        private void bodyH_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (body.Visibility == Visibility.Visible)
                body.Visibility = Visibility.Collapsed;
            else
                body.Visibility = Visibility.Visible;
        }

        private string FormatTextToJson(string text)
        {
            try
            {
                return JToken.Parse(text).ToString();
            }
            catch(Exception ex)
            {
                return text;
            }
        }

        private void TextBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (response.Visibility == Visibility.Visible)
                response.Visibility = Visibility.Collapsed;
            else
                response.Visibility = Visibility.Visible;
        }

        private void AddHeader_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(HeaderName.Text) || string.IsNullOrWhiteSpace(HeaderValue.Text))
                return;
            
            var result = WebHandler.AddHeader(HeaderName.Text, HeaderValue.Text);
            if (!result)
                return;
            Headers.Add(new Tuple<string, string>(HeaderName.Text, HeaderValue.Text));
            HeaderName.Text = string.Empty;
            HeaderValue.Text = string.Empty;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var tuple = (e.Source as FrameworkElement).DataContext;
            var result = WebHandler.RemoveHeader((tuple as Tuple<string, string>).Item1);
            if (!result)
                return;
            Headers.Remove(tuple as Tuple<string, string>);

        }
    }
}
